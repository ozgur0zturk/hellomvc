﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HelloWorld.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Mesaj = "Sed dolor ligula, posuere non dictum sit amet, euismod venenatis diam. Nunc quis posuere nisi? Sed faucibus turpis et dui venenatis scelerisque. Vestibulum erat magna; faucibus vel venenatis aliquam, dignissim ut metus. Nunc turpis orci, commodo ac augue in, volutpat mollis enim. Duis elementum leo mi, condimentum tristique quam iaculis venenatis! Aenean vel sem posuere orci pellentesque gravida. Aenean vel pellentesque sem, vitae faucibus dolor. Sed in justo pharetra est facilisis eleifend. In ac lacinia sapien. Nunc tincidunt, risus eu rutrum congue, neque lectus sollicitudin risus, et iaculis ante massa id urna. Praesent id ligula gravida, fringilla velit nec, imperdiet tortor. Nulla ac ultricies sem. Etiam non egestas sapien. Praesent eu dolor non arcu accumsan pharetra ut non diam. Nunc eleifend dignissim sollicitudin. Fusce placerat arcu mi, in mattis ex egestas tristique. Duis auctor venenatis feugiat. Ut at sapien consequat; fermentum dolor a, consequat mi. Donec felis libero, porttitor in neque et, pellentesque blandit est. Pellentesque metus tortor, maximus condimentum erat et, porttitor lobortis urna. Nulla auctor turpis sit amet libero bibendum volutpat. Nunc gravida facilisis hendrerit. Aenean at turpis urna. Etiam lacinia eros vitae enim scelerisque ultricies.Sed eu purus molestie, congue mi sed, finibus risus? Praesent maximus laoreet odio, ac porta tellus ultricies tincidunt.";
            return View();
        }

        public ActionResult Hakkimizda()
        {
            ViewBag.Mesaj = "Nunc in lacus et velit placerat tristique. Donec bibendum scelerisque leo vitae tempus. Etiam a lobortis enim. Etiam pretium purus urna, quis pulvinar metus consequat vitae. Nam blandit ullamcorper sodales. Nulla congue mollis sollicitudin. Phasellus ut vulputate est. Curabitur eget libero eu nibh sagittis suscipit at non quam. Mauris ex nibh, vulputate eu molestie eget, pretium quis diam. Duis sodales metus non ipsum suscipit; vitae dapibus risus lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus.Nunc metus mauris, feugiat in lectus vel; tincidunt sollicitudin sapien.Vestibulum placerat dolor non quam dapibus euismod.Duis lobortis non velit vitae ornare. Suspendisse fermentum sit amet tellus vestibulum pharetra.Fusce ac nisi et justo elementum elementum condimentum bibendum velit. Proin feugiat elit quis elit semper, ut aliquam dui ultrices.Nunc rhoncus, mauris id cursus tristique, massa odio dapibus libero, quis euismod tortor mi non libero.Maecenas ornare suscipit diam ac sollicitudin.";
            return View();
        
        }

        public string Beykoz()
        {
            return "Hello Beykoz";
        }
    }
}